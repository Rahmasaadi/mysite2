from django.contrib import admin
from .models import Person, User_Profile, Tumor, Doctor
# Register your models here.

admin.site.register(Person)
admin.site.register(Tumor)
admin.site.register(Doctor)
admin.site.register(User_Profile)
