from django.db import models

from django.contrib.auth.models import User
# Create your models here.


class Person(models.Model):
    F = "female"
    M = "male"
    GENDER_TYPES = (
        (M, 'male'),
        (F, 'female'),
    )
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    sex = models.CharField(choices=GENDER_TYPES, max_length=20, default=M)
    age = models.IntegerField()


class Tumor(models.Model):
    def content_file_name(instance, filename):
        return '/'.join(['photo', instance.person_id.first_name+""+instance.person_id.last_name, filename])

    person_id = models.ForeignKey(Person, on_delete=models.CASCADE)
    mri_image = models.ImageField(
        upload_to=content_file_name, blank=True, null=True)
    mri_mask = models.ImageField(
        upload_to=content_file_name, blank=True, null=True)
    mri_contour = models.ImageField(
        upload_to=content_file_name, blank=True, null=True)
    has_tumor = models.BooleanField(default=False)
    tumor_size = models.IntegerField(default=0)
    date_of_uploaded = models.DateTimeField(auto_now=True)


class User_Profile(models.Model):
    Registerator = "registerator"
    Specialist = "specialist"
    Radiologist = "radiologist"

    ACCOUNT_TYPES = (
        (Registerator, "Registerator"),
        (Specialist, "Specialist"),
        (Radiologist, "Radiologist"),

    )
    account_type = models.CharField(
        max_length=15, choices=ACCOUNT_TYPES, default=Registerator
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    phone = models.IntegerField(blank=True, null=True)
    token = models.CharField(blank=True, null=True, max_length=100)

    def __str__(self):
        return self.user.username


class Doctor(models.Model):
    tumor_id = models.ForeignKey(Tumor, on_delete=models.CASCADE)
    first_name = models.ForeignKey(User, on_delete=models.CASCADE)
    notes = models.TextField(null=True, blank=True)
