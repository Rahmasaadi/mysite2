from django.shortcuts import render
from .models import Person, Tumor, Doctor, User_Profile
import cv2
from .apps import App1Config
import numpy as np

from PIL import Image
from io import BytesIO
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import InMemoryUploadedFile
import os
import sys
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.db.models import Q
from django.conf import settings
from django.core.mail import send_mail
from django.core.mail import EmailMessage, get_connection
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import logout
import uuid
from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404
from django.contrib import messages

classifier = App1Config.classification_model
segmenter = App1Config.segmentaion_model
size = (256, 256)


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login/')


def login_view(request):
    username = None
    password = None
    if request.method == 'POST' and 'submitlogin' in request.POST:
        username = request.POST['uname']
        password = request.POST['pass']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            if user.is_staff == False:
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                login(request, user)
                return HttpResponseRedirect('/signup/')
        else:
            messages.error(
                request, 'Your Username Or Password Is not avilable')
            return render(request, 'loggin.html')
    else:
        return render(request, 'loggin.html')


def signup(request):
    if request.method == 'POST' and 'submitprofile' in request.POST:
        username = request.POST['uname']
        first_name = request.POST['fname']
        last_name = request.POST['lname']
        email = request.POST['email']

        pass1 = request.POST['pass1']
        pass2 = request.POST['pass2']
        account_type = request.POST['type']

        try:
            objuser = get_object_or_404(User, username=username)
            if objuser is not None:
                messages.error(
                    request, 'user name already exist , pleas enter anthor username')
                return render(request, 'signup.html')

        except:
            user = User(username=username, first_name=first_name,
                        last_name=last_name, email=email)
            if pass1 != pass2:
                messages.error(
                    request, 'passwoed 1 is no much with password 2')
                return render(request, 'signup.html')
            else:
                password = pass1
                user.set_password(password)
                user.save()
                obj = User_Profile(user=user,
                                   account_type=account_type)
                obj.save()
                user = authenticate(username=username, password=password)
                login(request, user)
                return HttpResponseRedirect('/signup/')
    else:
        return render(request, 'signup.html')


def deleteimgsp(request, id):
    tumor_deleted = Tumor.objects.get(id=id)
    person = tumor_deleted.person_id
    tumor_deleted.delete()
    tumor = Tumor.objects.filter(person_id=person)
    if tumor.exists():
        context = {
            'pro': person,
            'tumor': tumor,
            'doctor': Doctor.objects.filter(tumor_id__in=tumor)
        }
        return render(request, 'search.html', context)
    else:
        return HttpResponseRedirect('/speciallist/')


def deleteimg(request, id):
    list_id = []
    tumor_deleted = Tumor.objects.get(id=id)
    pid = tumor_deleted.person_id
    minute = tumor_deleted.date_of_uploaded.minute
    minute2 = minute+1
    minute3 = minute-1
    list_id.append(minute)
    list_id.append(minute2)
    if minute != 0:
        list_id.append(minute3)
    tumor_deleted.delete()
    tumor = Tumor.objects.filter(date_of_uploaded__minute__in=list_id)
    print(tumor)
    if tumor.exists():
        context = {
            'pid': pid,
            'pro': tumor,
        }

        return render(request, 'addmriimage.html', context)
    else:

        return HttpResponseRedirect('/mripatients/')


def addmriimage(request, id):
    profile = User_Profile.objects.get(user=request.user)
    if profile.account_type == 'radiologist':
        list_id = []
        if request.method == 'POST' and 'addmriimage' in request.POST:
            pid = id
            obj1 = Person.objects.get(id=pid)
            files = request.FILES.getlist('mri_image')
            for i in files:
                im_tif = cv2.imdecode(np.fromstring(
                    i.read(), np.uint8), cv2.IMREAD_UNCHANGED)
                base_dir = os.path.dirname(
                    os.path.dirname(os.path.abspath(__file__)))
                image_path1 = os.path.join(base_dir, 'app1', 'tif.tif')
                cv2.imwrite(image_path1, im_tif)
                im_tif = Image.open(image_path1)
                resolution = im_tif.info.get('dpi', (72, 72))
                png = im_tif.convert("RGBA")
                png.info['dpi'] = resolution
                image_path2 = os.path.join(base_dir, 'app1', 'tiffff.png')
                png.save(image_path2, format="PNG")
                im_png = Image.open(image_path2)
                buffer = BytesIO()
                im_png.save(buffer, format='png')
                buffer_v = ContentFile(buffer.getvalue())
                image_fil = InMemoryUploadedFile(
                    buffer_v, None, 'tiftopng.png', 'image/png', sys.getsizeof(buffer), None)
                obj = Tumor.objects.create(person_id=obj1)
                obj.mri_image.save('mri_img.png', image_fil)
                obj.save()
                obj2 = Tumor.objects.get(id=obj.id)
                imtif = cv2.imread(image_path1)
                im_resized = cv2.resize(imtif, (256, 256))
                im_resized = (im_resized / 255.0)
                im_ex = np.expand_dims(im_resized, axis=0)
                prediction = classifier.predict(im_ex)
                has_tumor = bool(prediction.round()[0][0])
                tumor_mask = segmenter.predict(im_ex)
                tumor_mask = (np.squeeze(tumor_mask)
                              > .5).astype("int32")*255
                tumor_mask = tumor_mask.astype("uint8")
                image_path3 = os.path.join(base_dir, 'app1', 'image2.png')
                cv2.imwrite(image_path3, tumor_mask)
                ph = Image.open(image_path3)
                img_io = BytesIO()
                ph.save(img_io, format='png')
                buffer_value = ContentFile(img_io.getvalue())
                image_file = InMemoryUploadedFile(
                    buffer_value, None, 'mask.png', 'image/png', sys.getsizeof(img_io), None)
                obj2.mri_mask.save('mask.png', image_file)
                obj2.save()
################################## add contour################################################
                path = 'media/'+str(obj2.mri_mask)
                path1 = os.path.join(base_dir, str(path))
                ir = cv2.imread(path1)
                gray = cv2.cvtColor(
                    ir, cv2.COLOR_BGR2GRAY).astype(np.uint8)
                thresh_value, thresh = cv2.threshold(
                    gray, 127, 255, cv2.THRESH_BINARY)
                contours, hierachy = cv2.findContours(
                    thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
                imagecopy2 = imtif.copy()
                if len(contours) > 0:
                    x, y, w, h = cv2.boundingRect(contours[0])
                    imgrec = cv2.rectangle(
                        imagecopy2, (x, y), (x+w, y+h), (0, 0, 255), 3)
                    image_path4 = os.path.join(
                        base_dir, 'app1', 'contoourimage2.png')
                    cv2.imwrite(image_path4, imgrec)
                    con = Image.open(image_path4)
                    con_io = BytesIO()
                    con.save(con_io, format='png')
                    buffer_valuec = ContentFile(con_io.getvalue())
                    con_file = InMemoryUploadedFile(
                        buffer_valuec, None, 'con.png', 'con/png', sys.getsizeof(con_io), None)
                    obj2.mri_contour.save('con.png', con_file)
                    obj2.save()
                    os.remove(image_path4)
                else:
                    obj2.mri_contour.save('con.png', image_file)
                    obj2.save()
                os.remove(image_path3)
                os.remove(image_path1)
                os.remove(image_path2)
                if has_tumor:
                    p = np.sum(tumor_mask == 255)
                    tumor_size = np.sqrt(p)*0.264
                else:
                    tumor_size = 0
                obj2.has_tumor = has_tumor
                obj2.tumor_size = tumor_size
                obj2.save()
                list_id.append(obj2.id)
            pid = obj1.id
            contex = {
                'pid': pid,
                'pro': Tumor.objects.filter(id__in=list_id),
            }
            return render(request, 'addmriimage.html', contex)
        else:
            return render(request, 'addmriimage.html')
    else:
        return render(request, 'noparrmission.html')


@login_required(login_url='/login/')
def index(request):
    user = User.objects.get(id=request.user.id)
    userprofile = User_Profile.objects.get(user=request.user)
    context = {
        'username': user,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'email': user.email,
        'date_joined': user.date_joined,
        'account_type': userprofile.account_type,
    }
    return render(request, 'index.html', context)


def search(request):
    searchid = None
    if 'search' in request.GET:
        searchid = request.GET['search']
        if searchid.isdigit():
            try:
                pro = get_object_or_404(Person, id=searchid)
                tumor = Tumor.objects.filter(person_id=searchid)
                context = {
                    'pro': pro,
                    'tumor': tumor,
                    'doctor': Doctor.objects.filter(tumor_id__in=tumor)
                }
                return render(request, 'search.html', context)
            except:
                messages.error(request, 'ID is not found')
                patients = Person.objects.all()
                context = {
                    "patient": patients
                }
            return render(request, 'speciallist.html', context)

        else:
            patients = Person.objects.all()
            patients = patients.filter(
                Q(first_name__icontains=searchid) | Q(last_name__icontains=searchid))
            context = {
                "patient": patients
            }
            return render(request, 'speciallist.html', context)


def searchid(request, id):
    tumor = Tumor.objects.filter(person_id=id)
    context = {
        'pro': Person.objects.get(id=id),
        'tumor': Tumor.objects.filter(person_id=id),
        'doctor': Doctor.objects.filter(tumor_id__in=tumor)
    }
    return render(request, 'search.html', context)


def speciallist(request):
    profile = User_Profile.objects.get(user=request.user)
    if profile.account_type == 'specialist':
        patients = Person.objects.all()
        context = {
            "patient": patients
        }
        return render(request, 'speciallist.html', context)
    else:
        return render(request, 'noparrmission.html')


def note(request, id):
    if request.method == 'POST' and 'submit' in request.POST:
        obj = Doctor(tumor_id=Tumor.objects.get(
            id=id), first_name=request.user, notes=request.POST['message'])
        obj.save()
    person_id = Tumor.objects.get(id=id).person_id
    tumor = Tumor.objects.filter(person_id=person_id.id)
    context = {
        'pro': Person.objects.get(id=person_id.id),
        'tumor': Tumor.objects.filter(person_id=person_id.id),
        'doctor': Doctor.objects.filter(tumor_id__in=tumor)
    }
    return render(request, 'search.html', context)


def GeneratePdf(request):
    person_id = None
    if request.method == 'POST' and 'pdfsubmit' in request.POST:
        person_id = request.POST['search']
        tumor = Tumor.objects.filter(person_id=person_id)
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="report.pdf"'
        template_path = 'pdf.html'
        template = get_template(template_path)
        context = {
            'pro': Person.objects.get(id=person_id),
            'tumor': Tumor.objects.filter(person_id=person_id),
            'doctor': Doctor.objects.filter(tumor_id__in=tumor)
        }
        html = template.render(context)
        pdf = pisa.CreatePDF(html, dest=response,
                             encoding='utf-8', link_callback=fetch_resources)

        css_file = os.path.join(settings.STATIC_ROOT, 'css/style.css')
        with open(css_file) as f:
            css = f.read()
        if pdf.err:
            return HttpResponse('We had some errors <pre>' + html + '</pre>')
    return response


def fetch_resources(uri, rel):

    if uri.lower().endswith(".tif"):
        with Image.open(uri) as tif:
            resolution = tif.info.get('dpi', (72, 72))
            png = tif.convert("RGBA")
            png.info['dpi'] = resolution
            png_bytes = BytesIO()
            png.save(png_bytes, format="PNG")
            png_bytes.seek(0)
        return png_bytes.getvalue()

    else:
        path = os.path.join(settings.STATIC_ROOT,
                            uri.replace(settings.STATIC_URL, ""))
        return path


def patients(request):
    profile = User_Profile.objects.get(user=request.user)
    if profile.account_type == 'registerator':
        patients = Person.objects.all()
        context = {
            "patient": patients
        }
        return render(request, 'patients.html', context)
    else:
        return render(request, 'noparrmission.html')


def patient(request, id):

    patient = Person.objects.get(id=id)
    tumor = Tumor.objects.filter(person_id=patient)
    doctor = Doctor.objects.filter(tumor_id__in=tumor)
    context = {
        "patient": patient,
        "tumor": tumor,
        "doctor": doctor,
    }
    return render(request, 'patient.html', context)


def addpatient(request):
    fname = None
    lname = None
    sex = None
    age = None

    if request.method == 'POST' and 'addsubmit' in request.POST:

        if 'fname' in request.POST and 'lname' in request.POST and 'sex' in request.POST and 'age' in request.POST:
            fname = request.POST['fname']
            lname = request.POST['lname']
            sex = request.POST['sex']
            age = request.POST['age']
            try:
                obj1 = Person.objects.get(
                    first_name=fname, last_name=lname, sex=sex)
                print("qqqqqqqqq", obj1)
            except:
                obj1 = Person(first_name=fname,
                              last_name=lname, sex=sex, age=age)
                obj1.save()
        pid = obj1.id
        context = {
            'pid': pid,
        }
        return HttpResponseRedirect('/patients/')
    else:
        return render(request, 'addpatient.html')


def deletepatient(request, id):
    obj = Person.objects.get(id=id)
    obj.delete()

    return HttpResponseRedirect('/patients/')


def updatepatient(request, id):
    if request.method == 'POST' and "updatesubmit" in request.POST:
        obj = Person.objects.get(id=id)
        obj.first_name = request.POST['fname']
        obj.last_name = request.POST['lname']
        obj.sex = request.POST['sex']
        obj.age = request.POST['age']
        obj.save()
        return HttpResponseRedirect('/patients/')
    else:
        obj = Person.objects.get(id=id)
        context = {
            'person': obj,
        }
        return render(request, 'updatepatient.html', context)


def editprofile(request):
    if request.method == 'POST' and 'Editetprofile' in request.POST:
        userobj = User.objects.get(id=request.user.id)
        userprofile = User_Profile.objects.get(user=request.user)
        username = request.POST['uname']
        first_name = request.POST['fname']
        last_name = request.POST['lname']
        email = request.POST['email']

        account_type = request.POST['type']
        oldpass = request.POST['oldpass']
        newpass = request.POST['newpass']
        if 'oldpass' in request.POST and 'newpass' in request.POST:
            if userobj.check_password(oldpass):
                userobj.set_password(newpass)
                userobj.save()
        userobj.username = username
        userobj.first_name = first_name
        userobj.last_name = last_name
        userobj.email = email
        userobj.save()

        userprofile.account_type = account_type
        userprofile.save()
        user = authenticate(username=username, password=newpass)
        login(request, user)
        return HttpResponseRedirect('/')
    else:
        user = User.objects.get(id=request.user.id)
        userprofile = User_Profile.objects.get(user=request.user)
        context = {
            'user': user,
            'userprofile': userprofile,
        }
        return render(request, 'editprofile.html', context)


def forgetpassword(request):
    if request.method == 'POST' and 'submitloginforgetpass' in request.POST:
        uname = request.POST['uname']
        try:
            user = User.objects.filter(username=uname).first()
            profile = User_Profile.objects.get(user=user)
        except:
            messages.error(request, 'The username is not available')
            return render(request, 'forgetpassword.html')
        token = str(uuid.uuid4())
        profile.token = token
        profile.save()
        sendemail(email=user.email, token=profile.token)
        messages.success(
            request, 'The reset passowrd link was sent to your email.Please check')
        return render(request, 'forgetpassword.html')
    else:
        return render(request, 'forgetpassword.html')


def sendemail(email, token):
    subject = 'your forget password link'
    message = f'Hi,click on the link to reset your password http://127.0.0.1:8000/changepassword/{token}/'
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [email]
    emailmessage = EmailMessage(subject, message, email_from, recipient_list)
    emailmessage.send()
    return True


def changepassword(request, token):
    if request.method == 'POST' and 'changepasswordsubmi' in request.POST:
        pass1 = request.POST['pass1']
        pass2 = request.POST['pass2']
        if pass1 != pass2:
            messages.error(request, 'passwoed 1 is no much with password 2')
            return render(request, 'changepassword.html')
        else:
            userprofile = User_Profile.objects.get(token=token)
            user = User.objects.get(id=userprofile.user.id)
            user.set_password(pass1)
            user.save()
            return HttpResponseRedirect('/')
    else:
        return render(request, 'changepassword.html')


def mripatients(request):
    profile = User_Profile.objects.get(user=request.user)
    if profile.account_type == 'radiologist':
        if request.method == 'POST' and 'tt' in request.POST and 'search' in request.POST:
            searchid = request.POST['search']
            if searchid.isdigit():
                try:
                    pro = get_object_or_404(Person, id=searchid)
                    patient = Person.objects.filter(id=searchid)
                    context = {
                        "patient": patient,
                    }
                    return render(request, 'mripatients.html', context)
                except:
                    messages.error(request, 'ID is not found')
                    return HttpResponseRedirect('/mripatients/')
            else:
                patients = Person.objects.all()
                patients = patients.filter(
                    Q(first_name__icontains=searchid) | Q(last_name__icontains=searchid))
                context = {
                    "patient": patients
                }
                return render(request, 'mripatients.html', context)
        else:
            patients = Person.objects.all()
            context = {
                "patient": patients
            }
            return render(request, 'mripatients.html', context)
    else:
        return render(request, 'noparrmission.html')
