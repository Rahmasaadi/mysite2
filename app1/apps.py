from django.apps import AppConfig
from django.conf import settings
import tensorflow as tf
from keras import backend as k

smooth = 100


def dice_coef(y_true, y_pred):
    y_true = tf.flatten(y_true)
    y_pred = tf.flatten(y_pred)
    intersection = tf.sum(y_true * y_pred)
    union = tf.sum(y_true) + tf.sum(y_pred)
    return (2.0 * intersection + smooth) / (union + smooth)


def dice_coef_loss(y_true, y_pred):
    return 1 - dice_coef(y_true, y_pred)


def bce_dice_loss():
    def loss(y_true, y_pred):
        bce = tf.keras.losses.BinaryCrossentropy(from_logits=True)
        return dice_coef_loss(y_true, y_pred) + bce(y_true, y_pred)
    return loss


class App1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app1'
    classification_model = tf.keras.models.load_model(
        settings.DEEP_LEARNING_DIR+'/brainMRI_Classifer.hdf5')
    segmentaion_model = tf.keras.models.load_model(
        settings.DEEP_LEARNING_DIR+'/brainMRI_Segment.hdf5', compile=False)
