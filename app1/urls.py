from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),  # 1
    path('search/', views.search, name='search'),  # 3
    path('searchid/<int:id>/', views.searchid, name='searchid'),  # 2
    path('search/<int:id>/', views.note, name='note'),  # 3
    path('pdfviews/', views.GeneratePdf, name='pdfviews'),  # 7
    path('signup/', views.signup, name='signup'),  # 2
    path('login/', views.login_view, name='login'),  # 2
    path('logout/', views.logout_view, name='logout'),  # 1
    path('patients/', views.patients, name='patients'),  # 2
    path('patient/<int:id>/', views.patient, name='patient'),  # 1
    path('addpatient/', views.addpatient, name='addpatient'),  # 3
    path('addmriimage/<int:id>/', views.addmriimage, name='addmriimage'),  # 10
    path('speciallist/', views.speciallist, name='speciallist'),  # 1
    path('deletepatient/<int:id>/', views.deletepatient, name='deletepatient'),  # 1
    path('updatepatient/<int:id>/', views.updatepatient, name='updatepatient'),  # 2
    path('editprofile/', views.editprofile, name='editprofile'),  # 2
    path('forgetpassword/', views.forgetpassword, name='forgetpassword'),  # 1
    path('changepassword/<token>/', views.changepassword,
         name='changepassword'),  # 3
    path('mripatients/', views.mripatients, name='mripatients'),  # 1
    path('deleteimg/<int:id>/', views.deleteimg, name='deleteimg'),  # 1
    path('deleteimgsp/<int:id>/', views.deleteimgsp, name='deleteimgsp')  # 1


]
# apache2 + aws  10
